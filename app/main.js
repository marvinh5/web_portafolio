"use strict";
/**
 * Created by marvinsena on 5/13/16.
 */
//import 'zone.js';
require('../node_modules/zone.js');
require('reflect-metadata');
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var app_component_1 = require('./components/app.component');
var core_1 = require('@angular/core');
core_1.enableProdMode();
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [core_1.provide(Window, { useValue: window })]);
//# sourceMappingURL=main.js.map