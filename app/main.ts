/**
 * Created by marvinsena on 5/13/16.
 */
//import 'zone.js';
import '../node_modules/zone.js'
import 'reflect-metadata';
import {bootstrap} from "@angular/platform-browser-dynamic"
import {AppComponent} from './components/app.component'
import {enableProdMode, provide} from '@angular/core';
enableProdMode();

bootstrap(AppComponent, [provide(Window, {useValue: window})]);