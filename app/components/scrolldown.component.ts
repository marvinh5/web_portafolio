/**
 * Created by marvinsena on 5/14/16.
 */

// <reference  path="typings/jquery/jquery.d.ts" />
import {Component, Input, ElementRef} from "@angular/core"
import {ClickOnRefActivate} from  "./navbar.component"
import {log} from "util";

declare var jQuery: JQueryStatic;

@Component({
    selector: "scrolldown",
    templateUrl:'app/components/scrolldown.component.html',
    inputs: ["target"],
    directives: [ClickOnRefActivate]
})


export class ScrollDownComponent {
    private current_position: number;
    private speed: number = 2;
    private direction: number = 1;
    private targetElement: any;
    private scrollInterval: any;
    @Input()
    private target: HTMLElement;

    constructor(private el: ElementRef){
      this.target = el.nativeElement;
    }

    scroll(){

        this.targetElement =  this.target
        console.log(this.target, jQuery(this.targetElement).offset().top);
        jQuery('html, body').animate({
            scrollTop: jQuery(this.targetElement).offset().top
        }, 500);

    }

    scrollingCallBack(){

        var targetPosition = this.targetElement.getBoundingClientRect().top;
        try {

            if (this.direction > 0 && (targetPosition <= 0 || (targetPosition - (this.speed * this.direction)) <= 0)) {
                window.scrollTo(0, targetPosition);
                clearInterval(this.scrollInterval);
            }
            else if (this.direction < 1 && (targetPosition >= 0 || (targetPosition + (this.speed * this.direction)) >= 0)) {
                window.scrollTo(0, targetPosition);
                clearInterval(this.scrollInterval);
            } else if (this.current_position >= document.body.scrollHeight) {
                window.scrollTo(0, targetPosition);
                clearInterval(this.scrollInterval);
            }
            this.current_position += this.speed * this.direction;
            window.scrollTo(0, this.current_position);
            this.speed += 0.05;
        }catch(exception) {
            window.scrollTo(targetPosition);
        }

    }

    check_is_chrome(): boolean{

        // Chrome 1+
        /*if(window && window.chrome)
            var isChrome = !!window.chrome && !!window.chrome.webstore;
        */
        return true
    }
}
