/**
 * Created by marvinsena on 5/13/16.
 */
import { Component, Output, EventEmitter} from "@angular/core"
import {Project} from "./project.component";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import 'rxjs/add/operator/share';

@Component(
    {
        selector:"thumbnail",
        templateUrl: "app/components/thumbnail.component.html",
        inputs: ["project"],
        styleUrls: ["app/components/thumbnail.component.css"]
    }
)

export class ProjectThumbnailComponent{
    project: Project;
    hovered: boolean = false;
    @Output() onClicked = new EventEmitter<boolean>();

    constructor(){
        //this.project.hidden = true;
    }

    click(){
        this.onClicked.emit(true);
        
    }

    show_thumbnail_icon(){
        this.project.hidden = false;
        this.hovered = true;
    }

    hide_thumbnail_icon()
    {
        this.project.hidden = true;
        this.hovered = false;
    }
}
