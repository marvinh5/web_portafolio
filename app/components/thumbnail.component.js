"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by marvinsena on 5/13/16.
 */
var core_1 = require("@angular/core");
require('rxjs/add/operator/share');
var ProjectThumbnailComponent = (function () {
    function ProjectThumbnailComponent() {
        this.hovered = false;
        this.onClicked = new core_1.EventEmitter();
        //this.project.hidden = true;
    }
    ProjectThumbnailComponent.prototype.click = function () {
        this.onClicked.emit(true);
    };
    ProjectThumbnailComponent.prototype.show_thumbnail_icon = function () {
        this.project.hidden = false;
        this.hovered = true;
    };
    ProjectThumbnailComponent.prototype.hide_thumbnail_icon = function () {
        this.project.hidden = true;
        this.hovered = false;
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ProjectThumbnailComponent.prototype, "onClicked", void 0);
    ProjectThumbnailComponent = __decorate([
        core_1.Component({
            selector: "thumbnail",
            templateUrl: "app/components/thumbnail.component.html",
            inputs: ["project"],
            styleUrls: ["app/components/thumbnail.component.css"]
        }), 
        __metadata('design:paramtypes', [])
    ], ProjectThumbnailComponent);
    return ProjectThumbnailComponent;
}());
exports.ProjectThumbnailComponent = ProjectThumbnailComponent;
//# sourceMappingURL=thumbnail.component.js.map