/**
 * Created by marvinsena on 5/15/16.
 */

import {Component, AfterViewInit, Inject} from "@angular/core"
import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[activateOnRef]' })
export class ClickOnRefActivate {
    private element: HTMLElement;

    constructor(el: ElementRef, private window: Window) {
        this.element = el.nativeElement;
        var this_class= this;
        el.nativeElement.addEventListener("click", function (event) {
            this_class.onClick(event)
        });
    }

    onClick(event){
        var elements = document.getElementsByClassName("active");
        for(var x =0; x< elements.length; x++){
            elements[x].removeAttribute("class")
        }
        this.element.parentElement.setAttribute("class", "active");
        event.preventDefault();
        this.window.location.replace(this.element.getAttribute("href"));
        this.window.location.hash = "";
    }
}

declare var jQuery: JQueryStatic;

@Component({
    selector: "nav-bar",
    templateUrl: "app/components/navbar.component.html",
    styleUrls:["css/bootstrap.css", "css/grayscale.css"],
    directives:[ClickOnRefActivate]
})



export class NavBarComponent implements AfterViewInit{

    ngAfterViewInit():any {
        var current_class = this;
        setInterval(function () {
            current_class.checkView()
        }, 600)
    }

    constructor(private el: ElementRef, private window: Window){
        this.window = window;
    }



    scrolled(event){
        console.log(event)
    }

    checkView() {
        var tags = this.el.nativeElement.getElementsByClassName("page-scroll");
        for (var x = 0; x < tags.length; x++)
        {
            var element = document.getElementById(tags[x].getAttribute("href").replace("#", ""));
            if(element.getBoundingClientRect().top < element.scrollHeight / 2){
                jQuery(tags).parents().removeClass("active");
                tags[x].parentElement.setAttribute("class", "active");
            }
        }
    }

}