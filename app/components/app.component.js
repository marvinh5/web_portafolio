"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by marvinsena on 5/13/16.
 */
var core_1 = require('@angular/core');
var project_component_1 = require("./project.component");
var project_service_1 = require("./project.service");
var http_1 = require('@angular/http');
var scrolldown_component_1 = require("./scrolldown.component");
var navbar_component_1 = require("./navbar.component");
var ProjectListComponent = (function () {
    function ProjectListComponent(projectService) {
        this.projectService = projectService;
    }
    ProjectListComponent.prototype.ngOnInit = function () {
        this.getProjects();
    };
    ProjectListComponent.prototype.getProjects = function () {
        var _this = this;
        this.projectService.getAllProjects().subscribe(function (projects) {
            _this.projects = projects.map(function (project) {
                return new project_component_1.Project(project.title, project.description, project.technologies, project.href, project.logo, project.id);
            });
        }, function (error) { return _this.errorMessage = error; });
    };
    ProjectListComponent = __decorate([
        core_1.Component({
            selector: 'project-list',
            template: "<div *ngFor=\"let project of projects\"><project [project]=\"project\"></project></div>",
            directives: [project_component_1.ProjectComponent],
            providers: [http_1.HTTP_PROVIDERS, http_1.JSONP_PROVIDERS, project_service_1.ProjectService]
        }), 
        __metadata('design:paramtypes', [project_service_1.ProjectService])
    ], ProjectListComponent);
    return ProjectListComponent;
}());
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: 'app/components/app.component.html',
            directives: [ProjectListComponent, scrolldown_component_1.ScrollDownComponent, navbar_component_1.NavBarComponent],
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//comment
//# sourceMappingURL=app.component.js.map