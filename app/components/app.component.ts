/**
 * Created by marvinsena on 5/13/16.
 */
import {Component, OnInit} from '@angular/core';
import {ProjectComponent, Project} from "./project.component"
import {ProjectService} from "./project.service"
import { HTTP_PROVIDERS, JSONP_PROVIDERS }    from '@angular/http';
import {ScrollDownComponent} from "./scrolldown.component";
import {NavBarComponent} from "./navbar.component";

@Component(
    {
        selector: 'project-list',
        template: `<div *ngFor="let project of projects"><project [project]="project"></project></div>`,
        directives: [ProjectComponent],
        providers: [HTTP_PROVIDERS, JSONP_PROVIDERS, ProjectService]

    }
)

class ProjectListComponent implements OnInit{
    projects: Project[];
    errorMessage: string;

    constructor(private projectService: ProjectService){

    }

    ngOnInit(){
        this.getProjects()
    }

    getProjects(){
        this.projectService.getAllProjects().subscribe(
            projects=>{
                this.projects = projects.map(function (project) {
                    return new Project(project.title, project.description, project.technologies, project.href, project.logo, project.id);
                });
            },
            error=>this.errorMessage = <any> error);
    }

    
}

@Component({
    selector: 'app',
    templateUrl: 'app/components/app.component.html',
    directives: [ProjectListComponent, ScrollDownComponent, NavBarComponent],
})

export class AppComponent{
    
}

//comment
