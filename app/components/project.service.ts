/**
 * Created by marvinsena on 5/13/16.
 */
import {Injectable, Injector} from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import {Project} from "./project.component"
import { Observable }     from 'rxjs/Observable';
import "rxjs/Rx"

@Injectable()
export class ProjectService{
    projects: Project[];
    private heroesUrl  = '/mockup.json';

    constructor(private http: Http){

    }

    getAllProjects(): Observable<Project[]>{
        return this.http.get(this.heroesUrl).map(this.extractData).catch(this.handleError);
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Response status: ' + res.status);
        }
        let body = res.json();
        return body;
    }
    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg = error.message || 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}