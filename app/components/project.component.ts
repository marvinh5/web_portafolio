/**
 * Created by marvinsena on 5/13/16.
 */
import {Component, AfterViewInit, ViewChild} from '@angular/core'
import {ProjectThumbnailComponent} from './thumbnail.component';
import {ModalComponent, Technology, Modal} from './modal.component';


export class Project{
    title:string;
    description: string;
    technologies: Technology[];
    href: string;
    logo: string;
    id: string;
    hidden: boolean = true;

    constructor(title, description, technologies, href, logo, id){
        
        this.technologies =  technologies.map(function(tech){
            return new Technology(tech.name, tech.src, tech.href)
        });
        this.title = title;
        this.description = description;
        this.href = href;
        this.logo = logo;
        this.id = id;
    }
}

var component_definition =  {
    selector: "project",
    templateUrl: "app/components/project.component.html",
    directives: [ProjectThumbnailComponent, ModalComponent],
    inputs: ["project"]
};

@Component(component_definition)
export class ProjectComponent{

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild(ProjectThumbnailComponent)
    private thumbnail: ProjectThumbnailComponent;

    constructor(){

    }

    showModal(){
        this.modal.showModal();
    }

}