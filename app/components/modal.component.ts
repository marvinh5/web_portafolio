/**
 * Created by marvinsena on 5/13/16.
 */
import {Component} from "@angular/core"
import {Project} from "./project.component";

export class Technology{


    constructor(public name:string, public src:string, public href:string){
    }
}

export class Modal{
    constructor(public id:string, public title:string, public description:string, public technologies: Technology[]){
       
    }
}
@Component({
    selector: "modal",
    templateUrl: "app/components/modal.component.html",
    styleUrls:["app/components/modal.component.css"],
    inputs: ["project"]
})

export class ModalComponent{

    modal_info: Modal;
    project: Project;
    show_modal: boolean = false;

    constructor(){
        this.modal_info = this.project;
    }

    showModal(){
        this.show_modal = true;
    }

    

}
